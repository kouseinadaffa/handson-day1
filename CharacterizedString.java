import java.util.Scanner;

public class CharacterizedString {
    public static void main(String[] args) {
        Scanner input1 = new Scanner(System.in);
        System.out.print("Ketik kata/kalimat, lalu tekan enter: ");
        String kalimat = input1.nextLine();
        String[] arrOfStr = kalimat.split("", 0);
        
        for(int i = 0; i < arrOfStr.length ; i++) {
            System.out.println("Karakter[" + i + "]: " + arrOfStr[i]);
        }
    }
}